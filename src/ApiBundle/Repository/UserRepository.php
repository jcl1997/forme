<?php

namespace ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UserRepository
 */
class UserRepository extends EntityRepository
{
    public function findOneByCpfOrEmail($cpf, $email)
    {
        $qb = $this
            ->createQueryBuilder('user');

        $qb->where($qb->expr()->orX(
            $qb->expr()->like('user.email', ':email'),
            $qb->expr()->like('user.cpf', ':cpf')
        ));

        $qb->setParameters(array(
            'cpf' => $cpf,
            'email' => $email
        ));

        $qb->setMaxResults(1);
        
        return $qb
            ->getQuery()
            ->getOneOrNullResult();
    }
}
