<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\FormType;
use ApiBundle\Entity\Form;
use ApiBundle\Utils\PaginatedCollection;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FormController extends FOSRestController
{
    /**
     * @Rest\Get("/api/form")
     */
    public function paginationAction(Request $request)
    {
        /**
         * @var \ApiBundle\Entity\User
         */
        $user = $this->getUser();

        $limit = 10;
        $parameters = array(
            ':userId' => $user->getId(),
        );
        $page = $request->query->get('page', '1');

        $qb = $this
            ->repositoryFrom()
            ->createQueryBuilder('f');

        $qb->select(array(
            'f.id',
            'f.name',
            't.id AS theme',
            't.name As themeName',
        ));

        $qb->join('f.theme', 't');

        $qb->where($qb->expr()->eq('f.user', ':userId'));
        if ($search = $request->query->get('search', false)) {
            $orx = $qb->expr()->orX();
            $orx->add($qb->expr()->like('f.name', ':search'));
            $orx->add($qb->expr()->like('t.name', ':search'));

            $qb->andWhere($orx);
            $parameters[':search'] = "%{$search}%";
        }

        $qb->orderBy('f.name', 'ASC');
        $qb->addOrderBy('themeName', 'ASC');

        $qb->setParameters($parameters);

        return new PaginatedCollection($qb, $limit, $page);
    }

    /**
     * @Rest\Post("/api/form")
     */
    public function createAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $user = $this->getUser();
        
        $form = $this->validateForm($data);
        if (!$form->isValid()) {
            return new View($form->getErrors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        /**
         * @var \ApiBundle\Entity\Form
         */
        $newForm = $form->getData();
        $newForm->setUser($user);

        $this->save($newForm);

        $json = $this->serviceFormJson();
        $json->setForm($newForm);

        return new View($json->get(), Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/api/form/{id}")
     */
    public function updateAction($id, Request $request)
    {
        $data = json_decode($request->getContent(), true);

        /**
         * @var \ApiBundle\Entity\Form
         */
        $formObject = $this
            ->repositoryFrom()
            ->find($id);

        if (!$formObject) {
            return new View('Registro não encontrado.', Response::HTTP_NOT_FOUND);
        }

        $form = $this->validateForm($data, $formObject);
        if (!$form->isValid()) {
            return new View($form->getErrors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $this->save($formObject);

        $json = $this->serviceFormJson();
        $json->setForm($formObject);

        return new View($json->get(), Response::HTTP_OK);
    }

    private function validateForm($data, Form $formObject = null)
    {
        $form = $this->createForm(FormType::class, $formObject);
        $form->submit($data);

        if ($formObject && $formObject->getId() != $data['id']) {
            $form
                ->get('id')
                ->addError(new FormError('Inválido.'));
        }

        $themeId = $data['theme'];
        $themeName = $data['themeName'];

        if (empty($themeId)) {
            /**
             * @var \ApiBundle\Entity\Theme
             */
            $theme = $this
                ->serviceThemeAutocomplete()
                ->get($themeName);
            $form->getData()->setTheme($theme);
        }
        
        return $form;
    }

    private function save(Form $form) {
        $theme = $form->getTheme();

        $em = $this->getDoctrine()->getManager();
        $em->persist($theme);
        $em->persist($form);
        $em->flush();
    }

    /**
     * @return \ApiBundle\Utils\Service\Theme\Autocomplete
     */
    private function serviceThemeAutocomplete()
    {
        return $this->get('api.theme.autocomplete');
    }

    /**
     * @return \ApiBundle\Utils\Service\Form\Json
     */
    private function serviceFormJson()
    {
        return $this->get('api.form.json');
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    private function repositoryFrom()
    {
        return $this
            ->getDoctrine()
            ->getRepository('ApiBundle:Form');
    }
}
