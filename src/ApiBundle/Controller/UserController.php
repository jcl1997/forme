<?php

namespace ApiBundle\Controller;

use ApiBundle\Entity\User;
use ApiBundle\Form\RegisterType;
use ApiBundle\Form\UserType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends FOSRestController
{
    /**
     * @Rest\Post("/register")
     */
    public function createAction(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $data = json_decode($request->getContent(), true);
        
        $user = new User();
        $form = $this->validateRegisterForm($data, $user);
        if (!$form->isValid()) {
            return new View($form->getErrors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $password = $passwordEncoder->encodePassword($user, $user->getPassword());
        $user->setPassword($password);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return new View(null, Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/api/user/{id}")
     */
    public function updateAction($id, Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $user = $this
            ->repositoryUser()
            ->find($id);

        if (!$user) {
            return new View('Registro não encontrado.', Response::HTTP_NOT_FOUND);
        }

        $form = $this->validateForm($data, $user);
        if (!$form->isValid()) {
            return new View($form->getErrors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $json = $this->serviceJson();
        $json->setUser($user);
        
        return new View($json->get(), Response::HTTP_OK);
    }

    private function validateForm($data, User $user = null)
    {
        $form = $this->createForm(UserType::class, $user);
        $form->submit($data);

        if ($user && $user->getId() != $data['id']) {
            $form
                ->get('id')
                ->addError(new FormError(
                    'Inválido.'
                ));
        }

        $now = new \DateTime('now');
        $now->sub(new \DateInterval('P18Y'));

        if ($now < $form->getData()->getDateOfBirth()) {
            $form
                ->get('dateOfBirth')
                ->addError(new FormError(
                    'O usuário deve ter idade minima de 18 anos.'
                ));
        }

        return $form;
    }

    private function validateRegisterForm($data, User $user)
    {
        $form = $this->createForm(RegisterType::class, $user);
        $form->submit($data);

        $now = new \DateTime('now');
        $now->sub(new \DateInterval('P18Y'));

        if ($now < $form->getData()->getDateOfBirth()) {
            $form
                ->get('dateOfBirth')
                ->addError(new FormError(
                    'O usuário deve ter idade minima de 18 anos.'
                ));
        }

        return $form;
    }

    /**
     * @return \ApiBundle\Utils\Service\User\Json
     */
    private function serviceJson()
    {
        return $this->get('api.user.json');
    }

    /**
     * @return \ApiBundle\Repository\UserRepository
     */
    private function repositoryUser()
    {
        return $this
            ->getDoctrine()
            ->getRepository('ApiBundle:User');
    }
}
