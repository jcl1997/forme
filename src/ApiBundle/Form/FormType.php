<?php

namespace ApiBundle\Form;

use ApiBundle\Entity\Theme;
use ApiBundle\Utils\Asserts;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'id',
                TextType::class,
                array('mapped' => false)
            )
            ->add(
                'name',
                TextType::class,
                Asserts::validateRequired()
            )
            ->add(
                'theme',
                EntityType::class,
                Asserts::buildConstraint(array(
                    'mapped' => false,
                    'class' => Theme::class
                ))
            )
            ->add(
                'themeName',
                TextType::class,
                Asserts::buildConstraint(array(
                    'required',
                    'mapped',
                ))
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\Form',
            'csrf_protection' => false,
        ));
    }
}
