<?php

namespace ApiBundle\Form;

use ApiBundle\Utils\Asserts;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'id',
                TextType::class,
                array('mapped' => false)
            )
            ->add(
                'name',
                TextType::class,
                Asserts::validateRequired()
            )
            ->add(
                'lastName',
                TextType::class,
                Asserts::validateRequired()
            )
            ->add(
                'dateOfBirth',
                DateType::class,
                Asserts::validateDate()
            )
            ->add(
                'cep',
                TextType::class,
                Asserts::validateRequired()
            )
            ->add(
                'cpf',
                TextType::class,
                Asserts::validateRequired()
            )
            ->add(
                'email',
                TextType::class,
                Asserts::validateRequired()
            )
            ->add(
                'gender',
                NumberType::class,
                Asserts::buildConstraint(array(
                    'required',
                    'choice' => array('1', '2')
                ))
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApiBundle\Entity\User',
            'csrf_protection' => false,
        ));
    }
}
