<?php

namespace ApiBundle\Utils;

use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class PaginatedCollection
{
    private $items;
    private $total;
    private $count;
    private $limit;
    private $start;
    private $end;
    private $pageCount;
    private $pageCurrent;

    public function __construct(QueryBuilder $queryBuilder, $limit, $pageCurrent, $items = null)
    {
        $start = 0;
        $end = 0;
        $totalItems = !empty($items) ? count($items) : 0;
        $pageCount = 1;

        if (empty($items)) {
            $adapter = new DoctrineORMAdapter($queryBuilder, true, false);
            $pagerfanta = new Pagerfanta($adapter);
            $pagerfanta->setMaxPerPage($limit);
            $pagerfanta->setCurrentPage($pageCurrent);

            $items = array();
            foreach ($pagerfanta->getCurrentPageResults() as $result) {
                $items[] = $result;
            }

            $start = 1 + ($pageCurrent*$limit)-$limit;
            $end = $start + count($items) - 1;
            $totalItems = $pagerfanta->getNbResults();
            $pageCount = ceil($totalItems/$limit);
        }

        $this->items = $items;
        $this->total = $totalItems;
        $this->count = count($items);
        $this->limit = $limit;
        $this->start = $start;
        $this->end = $end;
        $this->pageCount = $pageCount;
        $this->pageCurrent = $pageCurrent;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function setItems(array $items)
    {
        $this->items = $items;
    }
}
