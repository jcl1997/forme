<?php

namespace ApiBundle\Utils;

use Symfony\Component\Validator\Constraints as Assert;

class Asserts
{
    public static function buildConstraint($settings = array())
    {
        $options = array();
        $options['constraints'] = array();

        if (in_array('required', $settings)) {
            $options['constraints'][] = self::validateRequired()['constraints'];
        }

        if (isset($settings['length'])) {
            $min = $settings['length'][0];
            $max = $settings['length'][1];
            $options['constraints'][] = self::validateLength($min, $max)['constraints'];
        }

        if (isset($settings['choice'])) {
            $options['constraints']
                [] = self::validateChoice($settings['choice'])['constraints'];
        }

        if (in_array('date', $settings)) {
            $options['constraints'][] = self::validateDate()['constraints'];
        }

        if (in_array('mapped', $settings)) {
            $options['mapped'] = false;
        }

        if (isset($settings['class'])) {
            $options['class'] = $settings['class'];
        }

        return $options;
    }

    public static function validateRequired()
    {
        return array(
            'constraints' => new Assert\NotBlank(array(
                'message' => 'Este campo é obrigatório'
            )),
        );
    }

    public static function validateChoice($options)
    {
        return array(
            'constraints' => new Assert\Choice(array(
                'choices' => $options,
                'message' => 'Informe um valor válido',
            ))
        );
    }

    public static function validateEmail()
    {
        return array(
            'constraints' => new Assert\Email(array(
                'message' => 'Email inválido',
                'checkMX' => false,
            ))
        );
    }

    public static function validateDate()
    {
        return array(
            'constraints' => new Assert\Date(),
            'widget' => 'single_text',
            'invalid_message' => 'Informe uma data válida.',
        );
    }

    public static function validateLength($min, $max)
    {
        return array(
            'constraints' => new Assert\Length(array(
                'min' => $min,
                'max' => $max,
                'minMessage' => 'O campo pode ter no mínimo {{ limit }} caracteres.',
                'maxMessage' => 'O campo pode ter no máximo {{ limit }} caracteres.',
            )),
        );
    }

    public static function validateEntity(array $params)
    {
        $paramsDefault = array(
            'class' => null,
        );

        return array_merge($paramsDefault, $params);
    }
}
