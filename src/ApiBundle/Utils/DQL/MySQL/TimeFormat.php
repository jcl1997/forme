<?php

namespace ApiBundle\Utils\DQL\MySQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class TimeFormat extends FunctionNode
{
    public $stringValue;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->stringValue = $parser->StringPrimary();

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        $args = array(
            $sqlWalker->walkStringPrimary($this->stringValue),
            "'%H:%i'"
        );
        return sprintf('TIME_FORMAT(%s)', implode(', ', $args));
    }
}
