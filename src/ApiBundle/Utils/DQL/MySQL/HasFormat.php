<?php

namespace ApiBundle\Utils\DQL\MySQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class HasFormat extends FunctionNode
{
    public $stringValue;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->stringValue = $parser->StringPrimary();

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        $args = array(
            $sqlWalker->walkStringPrimary($this->stringValue)
        );

        return sprintf('IF(IFNULL(%s, 0) > 0, 1, 0)', implode(', ', $args));
    }
}
