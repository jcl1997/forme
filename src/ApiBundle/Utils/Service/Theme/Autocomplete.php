<?php

namespace ApiBundle\Utils\Service\Theme;

use ApiBundle\Entity\Theme;
use Doctrine\ORM\EntityManagerInterface;

class Autocomplete
{
    const NAME = 'api.theme.autocomplete';

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function get($name)
    {
        $theme = $this
            ->repositoryTheme()
            ->findOneByName($name);

        if (!$theme) {
            /**
             * @var \ApiBundle\Entity\Theme
             */
            $newTheme = new Theme();
            $newTheme->setName($name);
            
            return $newTheme;
        }

        return $theme;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    private function repositoryTheme()
    {
        return $this
            ->em
            ->getRepository('ApiBundle:Theme');
    }
}
