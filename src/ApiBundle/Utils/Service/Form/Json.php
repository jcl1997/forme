<?php

namespace ApiBundle\Utils\Service\Form;

use ApiBundle\Entity\Form;

class Json
{
    const NAME = 'api.form.json';

    /**
     * @var \ApiBundle\Entity\Form
     */
    private $form = null;
    
    public function setForm(Form $form)
    {
        $this->form = $form;

        return $this;
    }

    public function get()
    {
        return array(
            'id' => $this->form->getId(),
            'name' => $this->form->getName(),
            'theme' => $this->form->getTheme()->getId(),
            'themeName' => $this->form->getTheme()->getName(),
        );
    }
}