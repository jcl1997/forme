<?php

namespace ApiBundle\Utils\Service\Jwt;

use Firebase\JWT\JWT;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Token
{
    const name = 'api.jwt';

    private $key = null;
    private $environment = null;

    public function __construct(ContainerInterface $container, $key)
    {
        $this->key = $key;
        $this->environment = $container
            ->getParameter('kernel.environment');
    }

    public function encode($payload)
    {
        return JWT::encode($payload, $this->key);
    }

    public function decode($token)
    {
        return JWT::decode(
            $token,
            $this->key,
            array('HS256')
        );
    }

    public function setPayload(
        $exp = '1h',
        $params = []
    ) {
        $issuedAt = time();
        $expire = $issuedAt + $this->setExp($exp);
        $invalid = $issuedAt - 1;

        return array_merge(
            array(
                'iss' => $this->environment,
                'exp' => $expire,
                'iat' => $issuedAt,
                'nbf' => $invalid,
            ),
            $params   
        );
    }

    private function setExp($exp)
    {
        $oneHour = 60 * 60;
        $time = 0;
        if (strpos($exp, 'm') !== false) {
            $data = explode('m', $exp);
            $month = array_shift($data);
            $exp = implode($data);
            $time += $oneHour * 24 * 30 * $month;
        }

        if (strpos($exp, 'd') !== false) {
            $data = explode('d', $exp);
            $days = array_shift($data);
            $exp = implode($data);
            $time += $oneHour * 24 * $days;
        }

        if (strpos($exp, 'h') !== false) {
            $data = explode('h', $exp);
            $hous = array_shift($data);
            $time += $oneHour * $hous;
        }

        return $time;
    }
}
