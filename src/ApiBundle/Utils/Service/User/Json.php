<?php

namespace ApiBundle\Utils\Service\User;

use ApiBundle\Entity\User;

class Json
{
    const NAME = "api.user.json";

    /**
     * @var \ApiBundle\Entity\User
     */
    private $user = null;

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function get()
    {
        return array(
            'id' => $this->user->getId(),
            'name' => $this->user->getName(),
            'lastName' => $this->user->getLastName(),
            'dateOfBirth' => $this->user->getDateOfBirth(),
            'cep' => $this->user->getCep(),
            'cpf' => $this->user->getCpf(),
            'gender' => (int) $this->user->getGender(),
            'email' => $this->user->getEmail(),
        );
    }
}
