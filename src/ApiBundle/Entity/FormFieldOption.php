<?php

namespace ApiBundle\Entity;

/**
 * FormFieldOption
 */
class FormFieldOption
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \ApiBundle\Entity\FormField
     */
    private $formField;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $answer;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answer = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return FormFieldOption
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set formField.
     *
     * @param \ApiBundle\Entity\FormField|null $formField
     *
     * @return FormFieldOption
     */
    public function setFormField(\ApiBundle\Entity\FormField $formField = null)
    {
        $this->formField = $formField;

        return $this;
    }

    /**
     * Get formField.
     *
     * @return \ApiBundle\Entity\FormField|null
     */
    public function getFormField()
    {
        return $this->formField;
    }

    /**
     * Add answer.
     *
     * @param \ApiBundle\Entity\Answer $answer
     *
     * @return FormFieldOption
     */
    public function addAnswer(\ApiBundle\Entity\Answer $answer)
    {
        $this->answer[] = $answer;

        return $this;
    }

    /**
     * Remove answer.
     *
     * @param \ApiBundle\Entity\Answer $answer
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAnswer(\ApiBundle\Entity\Answer $answer)
    {
        return $this->answer->removeElement($answer);
    }

    /**
     * Get answer.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswer()
    {
        return $this->answer;
    }
}
