<?php

namespace ApiBundle\Entity;

/**
 * FormVersion
 */
class FormVersion
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $number;

    /**
     * @var \ApiBundle\Entity\Form
     */
    private $form;

    /**
     * @var \DateTime
     */
    private $createAt = 'CURRENT_TIMESTAMP';

    /**
     * @var bool
     */
    private $enabled = '0';

    /**
     * @var bool
     */
    private $finished = '0';

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number.
     *
     * @param string $number
     *
     * @return FormVersion
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number.
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set form.
     *
     * @param \ApiBundle\Entity\Form|null $form
     *
     * @return FormVersion
     */
    public function setForm(\ApiBundle\Entity\Form $form = null)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form.
     *
     * @return \ApiBundle\Entity\Form|null
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set createAt.
     *
     * @param \DateTime $createAt
     *
     * @return FormVersion
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt.
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set enabled.
     *
     * @param bool $enabled
     *
     * @return FormVersion
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled.
     *
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set finished.
     *
     * @param bool $finished
     *
     * @return FormVersion
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;

        return $this;
    }

    /**
     * Get finished.
     *
     * @return bool
     */
    public function getFinished()
    {
        return $this->finished;
    }
}
