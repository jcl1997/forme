<?php

namespace ApiBundle\Entity;

/**
 * Answer
 */
class Answer
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var \ApiBundle\Entity\Form
     */
    private $form;

    /**
     * @var \ApiBundle\Entity\FormField
     */
    private $formField;

    /**
     * @var \ApiBundle\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $formFieldOption;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->formFieldOption = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return Answer
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set form.
     *
     * @param \ApiBundle\Entity\Form|null $form
     *
     * @return Answer
     */
    public function setForm(\ApiBundle\Entity\Form $form = null)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form.
     *
     * @return \ApiBundle\Entity\Form|null
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set formField.
     *
     * @param \ApiBundle\Entity\FormField|null $formField
     *
     * @return Answer
     */
    public function setFormField(\ApiBundle\Entity\FormField $formField = null)
    {
        $this->formField = $formField;

        return $this;
    }

    /**
     * Get formField.
     *
     * @return \ApiBundle\Entity\FormField|null
     */
    public function getFormField()
    {
        return $this->formField;
    }

    /**
     * Set user.
     *
     * @param \ApiBundle\Entity\User|null $user
     *
     * @return Answer
     */
    public function setUser(\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \ApiBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add formFieldOption.
     *
     * @param \ApiBundle\Entity\FormFieldOption $formFieldOption
     *
     * @return Answer
     */
    public function addFormFieldOption(\ApiBundle\Entity\FormFieldOption $formFieldOption)
    {
        $this->formFieldOption[] = $formFieldOption;

        return $this;
    }

    /**
     * Remove formFieldOption.
     *
     * @param \ApiBundle\Entity\FormFieldOption $formFieldOption
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFormFieldOption(\ApiBundle\Entity\FormFieldOption $formFieldOption)
    {
        return $this->formFieldOption->removeElement($formFieldOption);
    }

    /**
     * Get formFieldOption.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFormFieldOption()
    {
        return $this->formFieldOption;
    }
}
