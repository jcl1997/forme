<?php

namespace ApiBundle\Entity;

/**
 * Form
 */
class Form
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \ApiBundle\Entity\Theme
     */
    private $theme;

    /**
     * @var \ApiBundle\Entity\User
     */
    private $user;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Form
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set theme.
     *
     * @param \ApiBundle\Entity\Theme|null $theme
     *
     * @return Form
     */
    public function setTheme(\ApiBundle\Entity\Theme $theme = null)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme.
     *
     * @return \ApiBundle\Entity\Theme|null
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set user.
     *
     * @param \ApiBundle\Entity\User|null $user
     *
     * @return Form
     */
    public function setUser(\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \ApiBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
