<?php

namespace ApiBundle\Entity;

/**
 * FormField
 */
class FormField
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $position;

    /**
     * @var \ApiBundle\Entity\Form
     */
    private $form;

    /**
     * @var \ApiBundle\Entity\FormFieldType
     */
    private $formFieldType;

    /**
     * @var \ApiBundle\Entity\FormVersion
     */
    private $formVersion;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return FormField
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position.
     *
     * @param string $position
     *
     * @return FormField
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set form.
     *
     * @param \ApiBundle\Entity\Form|null $form
     *
     * @return FormField
     */
    public function setForm(\ApiBundle\Entity\Form $form = null)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form.
     *
     * @return \ApiBundle\Entity\Form|null
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set formFieldType.
     *
     * @param \ApiBundle\Entity\FormFieldType|null $formFieldType
     *
     * @return FormField
     */
    public function setFormFieldType(\ApiBundle\Entity\FormFieldType $formFieldType = null)
    {
        $this->formFieldType = $formFieldType;

        return $this;
    }

    /**
     * Get formFieldType.
     *
     * @return \ApiBundle\Entity\FormFieldType|null
     */
    public function getFormFieldType()
    {
        return $this->formFieldType;
    }

    /**
     * Set formVersion.
     *
     * @param \ApiBundle\Entity\FormVersion|null $formVersion
     *
     * @return FormField
     */
    public function setFormVersion(\ApiBundle\Entity\FormVersion $formVersion = null)
    {
        $this->formVersion = $formVersion;

        return $this;
    }

    /**
     * Get formVersion.
     *
     * @return \ApiBundle\Entity\FormVersion|null
     */
    public function getFormVersion()
    {
        return $this->formVersion;
    }
}
