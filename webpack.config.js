const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  watchOptions: {
    poll: 1000
  },
  entry: [
    './web/assets/js/index.ts'
  ],
  output: {
    filename: 'bundle.[hash].js',
    chunkFilename: '[name].[hash].bundle.js',
    publicPath: '/build/',
    path: path.resolve(__dirname, 'web/build/')
  },
  module: {
    rules: [
      { test: /\.tsx?$/, exclude: /node_modules/, use: 'ts-loader' },
      { test: /\.ts$/, exclude: /node_modules/, loader: 'babel-loader' },
      { test: /\.css$/, exclude: /node_modules/, loader: ['style-loader', 'css-loader'] }
    ],
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
  },
  plugins: [
    new CleanWebpackPlugin([
      path.resolve(__dirname, 'web/build/')
    ], {
      exclude: ['.gitkeep']
    }),
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, 'app/Resources/views/index.html.twig'),
      template: path.resolve(__dirname, 'app/Resources/views/index.html.twig.template'),
      inject: 'body'
    }),
    new BrowserSyncPlugin({
      host: 'localhost',
      port: '3000',
      proxy: 'http://localhost:8000/'
    })
  ]
};
