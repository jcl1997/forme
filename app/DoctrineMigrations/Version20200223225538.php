<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200223225538 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $sql = <<<SQL
-- MySQL Workbench Synchronization
-- Generated: 2020-02-23 19:55
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: julio

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `form_version` 
DROP COLUMN `create_at`,
ADD COLUMN `create_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `number`,
ADD COLUMN `enabled` TINYINT(4) NOT NULL DEFAULT 0 AFTER `create_at`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
SQL;

        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
