<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200223232528 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $sql = <<<SQL
-- MySQL Workbench Synchronization
-- Generated: 2020-02-23 20:24
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: julio

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `form_version` 
DROP COLUMN `create_at`,
CHANGE COLUMN `number` `number` VARCHAR(45) NOT NULL DEFAULT '1' ,
CHANGE COLUMN `enabled` `enabled` TINYINT(1) NOT NULL DEFAULT 0 ,
ADD COLUMN `create_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `number`,
ADD COLUMN `finished` TINYINT(1) NOT NULL DEFAULT 0 AFTER `enabled`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
SQL;

        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
