<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200125145752 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `form_field` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `form_id` INT(11) NOT NULL,
  `form_version_id` INT(11) NOT NULL,
  `form_field_type_id` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `position` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_form_field_form_version1_idx` (`form_version_id` ASC),
  INDEX `fk_form_field_form1_idx` (`form_id` ASC),
  INDEX `fk_form_field_form_field_type1_idx` (`form_field_type_id` ASC),
  CONSTRAINT `fk_form_field_form_version1`
    FOREIGN KEY (`form_version_id`)
    REFERENCES `form_version` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_form_field_form1`
    FOREIGN KEY (`form_id`)
    REFERENCES `form` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_form_field_form_field_type1`
    FOREIGN KEY (`form_field_type_id`)
    REFERENCES `form_field_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
SQL;

        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
