<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200125150252 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `answer_has_form_field_option` (
  `answer_id` INT(11) NOT NULL,
  `form_field_option_id` INT(11) NOT NULL,
  PRIMARY KEY (`answer_id`, `form_field_option_id`),
  INDEX `fk_answer_has_form_field_option_form_field_option1_idx` (`form_field_option_id` ASC),
  INDEX `fk_answer_has_form_field_option_answer1_idx` (`answer_id` ASC),
  CONSTRAINT `fk_answer_has_form_field_option_answer1`
    FOREIGN KEY (`answer_id`)
    REFERENCES `answer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_answer_has_form_field_option_form_field_option1`
    FOREIGN KEY (`form_field_option_id`)
    REFERENCES `form_field_option` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
SQL;

        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
